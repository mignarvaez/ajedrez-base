package org.example;

enum estado{
    INICIADA,
    EN_ESPERA,
    FINALIZADA
}
public class Partida {

    private String estado;

    private Tablero tablero;

    public Partida(Tablero tablero){
        this.tablero = tablero;
    }
    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public Tablero getTablero() {
        return tablero;
    }

}
