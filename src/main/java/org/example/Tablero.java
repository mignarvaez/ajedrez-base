package org.example;

public class Tablero {

    public static final int NUMERO_COLUMNAS = 8;
    public static final int NUMERO_FILAS = 8;
    public static final int NUMERO_CASILLAS = 64;

    private Casilla[] casillas;


    public Tablero(){
        casillas = new Casilla[NUMERO_CASILLAS];
    }
    public void validarMovimiento(){

    }

    public void validarGanador(){

    }

    public Casilla[] getCasillas() {
        return casillas;
    }


}
