package org.example;

public class Peon extends Ficha{

    private boolean estadoInicial;


    @Override
    public void hacerMovimiento(){

    }

    public void coronar(){

    }

    public void validarMovimientoInicial(boolean estadoInicial) {
        this.estadoInicial = !estadoInicial;
    }

    public boolean isEstadoInicial() {
        return estadoInicial;
    }


}
